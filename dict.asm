%include "lib.inc"
%define OFFSET 8
section .text

global find_word

find_word:
    push rdi
    push rsi

    add rsi, OFFSET
    call string_equals

    pop rsi
    pop rdi
    
    test rax, rax
    jnz  .found

    .go_next:
        cmp qword[rsi], 0
        jz .end_of_list
        mov rsi, [rsi]
        jmp find_word

    .found:
        mov rax, rsi
        ret
        
    .end_of_list:
        xor rax, rax
        ret
